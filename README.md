Search Meetup Frankfurt Theme
=============================

This is the WordPress theme used for the [Search Meetup Frankfurt site](http://searchmeetupfrankfurt.de/).

It is a child theme for [Twenty Sixteen](https://wordpress.org/themes/twentysixteen/).
